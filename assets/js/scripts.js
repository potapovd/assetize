function selectAllCheckboxes(){
    console.log("select-all-action")
    var checked = !$(this).data('checked');
    $('input:checkbox').prop('checked', checked);
    $(this).val(checked ? 'uncheck all' : 'check all' )
    $(this).data('checked', checked);
}

$(document).ready(function () {

    //Selector Plugin  
    // no search box 
    $('.selector-action').select2({
        minimumResultsForSearch: Infinity
    });
    // with search box 
    $('.selector-action-search').select2({
        //minimumResultsForSearch: Infinity
    });


    //Calendar plugin  // $('.fa-calendar').datepicker({
    $('.calendar-action').datepicker({
        autoclose: true
    }).on('changeDate', function(ev){
        if(ev.currentTarget.dataset.calendarResult){
            res = "#"+ev.currentTarget.dataset.calendarResult;
            var newDate = new Date(ev.date).toLocaleTimeString([], {day:'2-digit',month:'2-digit', year:'numeric'});
            var dateArray = newDate.replace(/\,.*/, '').split('/')
            $(res).html( dateArray[1]+"/"+dateArray[0]+"/"+dateArray[2] );
            $('.calendar-action').datepicker('hide');
        }
    });

    //Input Cleaner
    $('span.clear-input').on("click",function(){
        $(this).prev().val("")
    })

    //Check and Uncheck all checkboxes
    $('.select-all-action').change(function() {
        selectAllCheckboxes()
    });
    $('button.select-all-action').on("click",function(){
        selectAllCheckboxes()
    })


    /* Page asset-serial-number - Serialgenerator */
    $("#serial-prexif, #serial-number, #serial-surfix").on("input",function(e){
        var prefix = $("#serial-prefix").val(),
            number = $("#serial-number").val(),
            suffix = $("#serial-suffix").val(),
            result = $("#serial-preview");
        result.val(prefix+"/"+number+"/"+suffix);
    })


});

// Custom Scroll
$(window).on("load",function(){
					
    $("body").mCustomScrollbar({
        theme:"minimal",
        axis:"y",
    });
    
});

